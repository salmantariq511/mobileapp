import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/service/api-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
menuList:any;

  constructor(private apiService:ApiServiceService){

  }

  ngOnInit(): void {
    this.apiService.getMenu().subscribe(res => {
      this.loadMenu(res);
    });
    
  }

  loadMenu(res) {
    this.menuList = res.result;
    console.log(this.menuList);
  }
}
