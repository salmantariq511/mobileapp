# MobileApp

Steps To Run The Project

1. run command "npm install" to install all the required packages.

2. run command "ng serve" to run the application.

3. put your browser in mobile mode by pressing "F12"

4. Go to localhost:(YourPort)/home to view the home page
